Sure, here are some synonyms for those phrases:

1. **According to popular belief:**
   - As commonly believed
   - As widely thought
   - As generally accepted
   - In the public eye
   - By common consensus

2. **According to a popular saying:**
   - As the saying goes
   - According to the adage
   - As the proverb states
   - In proverbial terms
   - By common expression

3. **According to lore:**
   - Based on legend
   - According to tradition
   - From folklore accounts 
   - As per myth 
   - In traditional tales

# Sun  2 Jun 00:04:05 CEST 2024 - Suggest some synonyms for ‘according to popular belief’ or ‘according to a popular saying’ or ‘according to lore’